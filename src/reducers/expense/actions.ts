import { CategoryItem } from './reducer';
export function addCategory(name: string) {
  return {
    type: "addCategory" as "addCategory",
    name,
  };
}
export function addItem(cat_id: number, name: string) {
  return {
    type: "addItem" as "addItem",
    cat_id,
    name,
  };
}
export function updateItem(item: CategoryItem) {
  return {
    type: 'updateItem' as 'updateItem',
    item
  }
 }
export type ActionType =
  | ReturnType<typeof addCategory>
  | ReturnType<typeof addItem>
  | ReturnType<typeof updateItem>;
