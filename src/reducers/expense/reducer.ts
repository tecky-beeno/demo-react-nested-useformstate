import { ActionType } from "./actions";
export interface State {
  categories: Record<number, Category>;
}
export interface Category {
  cat_id: number;
  cat_name: string;
  items: Record<number, CategoryItem>;
}
export interface CategoryItem {
  cat_id: number;
  item_id: number;
  item_name: string;
  quantity: number;
  price: number;
}
let initState: State = {
  categories: {
    1: {
      cat_id: 1,
      cat_name: "Food",
      items: {
        1: {
          cat_id: 1,
          item_id: 1,
          item_name: "milk",
          quantity: 1,
          price: 0,
        },
      },
    },
  },
};
export function reducer(state = initState, action: ActionType): State {
  switch (action.type) {
    case "addCategory": {
      let cat_id = Object.keys(state.categories).length + 1;
      return {
        ...state,
        categories: {
          ...state.categories,
          [cat_id]: {
            cat_id,
            cat_name: action.name,
            items: {},
          },
        },
      };
    }
    case "addItem": {
      let category = state.categories[action.cat_id];
      if (!category) {
        console.error("failed to find category by cat_id:", action.cat_id);
        return state;
      }
      let item_id = Object.keys(category.items).length + 1;
      return {
        ...state,
        categories: {
          ...state.categories,
          [action.cat_id]: {
            ...category,
            items: {
              ...category.items,
              [item_id]: {
                cat_id: action.cat_id,
                item_id,
                item_name: action.name,
                quantity: 1,
                price: 0,
              },
            },
          },
        },
      };
    }
    case "updateItem": {
      let item = action.item;
      let category = state.categories[action.item.cat_id];
      if (!category) {
        console.error("failed to find category by cat_id:", action.item.cat_id);
        return state;
      }
      return {
        ...state,
        categories: {
          ...state.categories,
          [category.cat_id]: {
            ...category,
            items: {
              ...category.items,
              [item.item_id]: item,
            },
          },
        },
      };
    }
    default:
      return state;
  }
}
