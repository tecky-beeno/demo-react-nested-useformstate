export function addItem(amount:number) {
  return { type: "addItem" as 'addItem',amount};
}
export function removeItem(id: number) {
  return { type: "removeItem" as 'removeItem', id };
}
export function updateItemName(id: number, name: string) {
  return { type: "updateItemName" as 'updateItemName', id, name };
}
export type ActionType =
  | ReturnType<typeof addItem>
  | ReturnType<typeof removeItem>
  | ReturnType<typeof updateItemName>;
