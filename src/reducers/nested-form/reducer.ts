import { ActionType } from "./actions";
export interface State {
  items: Record<number, Item>;
}
export interface Item {
  id: number;
  name: string;
  // isNewItem: boolean
}
let initState: State = {
  items: {},
};
export function reducer(state = initState, action: ActionType): State {
  switch (action.type) {
    case "addItem": {
      let items = state.items;
      for (let i = 0; i < action.amount; i++) {
        let id = Object.keys(items).length + 1;
        items = {
          ...items,
          [id]: { id, name: "" },
        };
      }
      return {
        ...state,
        items
      };
    }
    case "removeItem": {
      let items = { ...state.items };
      delete items[action.id];
      return { ...state, items };
    }
    case "updateItemName": {
      let item = state.items[action.id];
      if (!item) {
        item = {
          id: action.id,
          name: action.name,
        };
      } else {
        item = { ...item, name: action.name };
      }
      return {
        ...state,
        items: {
          ...state.items,
          [item.id]: item,
        },
      };
    }
    default:
      return state;
  }
}
