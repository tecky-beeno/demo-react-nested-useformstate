import { combineReducers, createStore } from "redux";
import { reducer as expenseReducer } from "./reducers/expense/reducer";
import { reducer as nestedFormReducer } from './reducers/nested-form/reducer';

let reducers = combineReducers({
  expense: expenseReducer,
  nestedForm: nestedFormReducer
});
export type RootState = ReturnType<typeof reducers>;

let store = createStore(reducers);
export default store;
