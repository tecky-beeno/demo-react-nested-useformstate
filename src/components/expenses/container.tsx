import React from "react";
import { useFormState } from "react-use-form-state";
import { addCategory } from "../../reducers/expense/actions";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store";
import ExpenseCategory from "./category";

function ExpenseContainer() {
  let categories = Object.values(
    useSelector((state: RootState) => state.expense.categories)
  );
  let [formState, { text }] = useFormState({
    cat_name: "",
  });
  let { cat_name } = formState.values;
  let dispatch = useDispatch();
  return (
    <div>
      <h1>Expenses</h1>
      {/*<form
        onSubmit={(event) => {
          event.preventDefault()
          dispatch(addCategory(cat_name));
        }}
      >*/}
      <input {...text("cat_name")} placeholder="New category name" />
      <button
        onClick={() => {
          dispatch(addCategory(cat_name));
          formState.reset();
          // formState.setField('cat_name','')
        }}
      >
        Add Category
      </button>
      <span>cat_name: {cat_name}</span>
      {/*</form>*/}
      <div>
        {categories.length} Categories
        {categories.map((cat) => (
          <ExpenseCategory cat_id={cat.cat_id} key={cat.cat_id} />
        ))}
      </div>
    </div>
  );
}
export default ExpenseContainer;
