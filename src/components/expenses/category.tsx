import React, { useMemo } from "react";
import { RootState } from "../../store";
import { useDispatch, useSelector } from "react-redux";
import { addItem } from "../../reducers/expense/actions";
import { useFormState } from "react-use-form-state";
import ExpenseItem from "./item";

function ExpenseCategory(prop: { cat_id: number }) {
  // let { category, items } = useSelector((state: RootState) => {
  let  category  = useSelector((state: RootState) => {
    let category = state.expense.categories[prop.cat_id];
    return category
    // let items = category ? Object.values(category.items) : [];
    // return { category, items };
  });

  // -- version 1 --
  // let items = Object.values(category.items)
  // -- version 2 --
  // let [items, setItems] = useState([] as CategoryItem[]);
  // useEffect(() => {
  //   setItems(Object.values(category.items));
  // }, [category]);
  // -- version 3 --
  let items = useMemo(() => Object.values(category.items), [category]);

  let dispatch = useDispatch();
  let [formState, { text }] = useFormState({
    item_name: "",
  });
  let { item_name } = formState.values;
  if (!category) {
    return <div>Error: failed to find category {prop.cat_id}</div>;
  }
  function saveAll() {
    console.log("save all items", items);
  }
  return (
    <div>
      <h2>
        Category: {category.cat_name}
        <button onClick={saveAll}>Save All Item</button>
      </h2>
      <input {...text("item_name")} placeholder="New item name" />
      <button
        onClick={() => {
          dispatch(addItem(prop.cat_id, item_name));
          formState.reset();
        }}
      >
        Add Item
      </button>
      <span>item_name: {item_name}</span>
      <div>
        {items.length} Items
        {items.map((item) => (
          <ExpenseItem
            cat_id={item.cat_id}
            item_id={item.item_id}
            key={item.item_id}
          />
        ))}
      </div>
    </div>
  );
}
export default ExpenseCategory;
