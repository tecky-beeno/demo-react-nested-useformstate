import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useFormState } from "react-use-form-state";
import { RootState } from "../../store";
import { updateItem } from "../../reducers/expense/actions";
import { CategoryItem } from "../../reducers/expense/reducer";

function ExpenseItem(prop: { cat_id: number; item_id: number }) {
  let item = useSelector(
    (state: RootState) =>
      state.expense.categories[prop.cat_id]?.items[prop.item_id]
  );
  let dispatch = useDispatch();
  let [formState, { text, number }] = useFormState(item, {
    onChange: (event, oldStateValues, newStateValues) => {
      console.log("new item values:", newStateValues);
      dispatch(updateItem(newStateValues as CategoryItem));
    },
  });
  if (!item) {
    return (
      <div>
        Failed to find item (cat_id: {prop.cat_id}, item_id: {prop.item_id})
      </div>
    );
  }
  return (
    <div>
      <h3>Item: {item.item_id}</h3>
      <input {...text("item_name")} />
      <input {...number("quantity")} />
      <button>Save</button>
    </div>
  );
}
export default ExpenseItem;
