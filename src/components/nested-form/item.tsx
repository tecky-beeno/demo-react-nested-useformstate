import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store";
import { updateItemName, removeItem } from "../../reducers/nested-form/actions";
export default function Item(prop: { id: number }) {
  let item = useSelector((state: RootState) => state.nestedForm.items[prop.id]);
  let dispatch = useDispatch();
  if (!item) {
    return <div>item {prop.id} not found</div>;
  }
  return (
    <div>
      <h2>item {prop.id}</h2>
      <button
        onClick={() => {
          console.log("save item to server:", item);
        }}
      >
        Save Item
      </button>
      <button
        onClick={() => {
          dispatch(removeItem(prop.id));
        }}
      >
        Delete Item
      </button>
      <input
        value={item.name}
        onChange={(e) => {
          dispatch(updateItemName(prop.id, e.target.value));
        }}
      />
    </div>
  );
}
