import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useFormState } from "react-use-form-state";
import { RootState } from "../../store";
import Item from "./item";
import { addItem } from "../../reducers/nested-form/actions";
export default function Container() {
  let items = Object.values(
    useSelector((state: RootState) => state.nestedForm.items)
  ).sort((a, b) => b.id - a.id);
  let [formState, { number }] = useFormState({ amount: 1 });
  let dispatch = useDispatch();
  function add() {
    dispatch(addItem(+formState.values.amount));
  }
  return (
    <div>
      <h1>Container</h1>
      <input {...number("amount")} />
      <button onClick={add}>Add Item</button>
      <p>{items.length} Items:</p>
      {items.map((item) => (
        <Item id={item.id} key={item.id} />
      ))}
    </div>
  );
}
